﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Rows: ");
            int rows = int.Parse(Console.ReadLine());
            Console.Write("Сolumns: ");
            int columns = int.Parse(Console.ReadLine());
            int[,] board = new int[rows, columns];
            int[,] boardTemp = new int[rows, columns];
            int[,] figure = new int[4, 4];
            Console.WriteLine("Задайте игрвое поле {0}X{1}:", rows, columns);
            Program.Read(board);
            Console.WriteLine("Задайте фигуру:");
            Program.Read(figure);
            List<int> rowscompleted = new List<int>();
            rowscompleted.Add(0);
            for (int rotate = 0; rotate < 4; rotate++)
            {
                Program.rotateFigure(figure);
                for (int j = 0; j <= columns - GetWidth(figure); j++)
                {
                    for (int i = rows; i >= GetHeight(figure); i--)
                    {
                        Array.Copy(board, boardTemp, board.Length);
                        Program.insertFigure(boardTemp, figure, j, i);
                        if (Program.checkOverlay(boardTemp)) continue;
                        //if (!Program.checkRules(boardTemp)) continue;   тут значит надеюсь будет метод, 
                        //                                                который определяет возможно ли так поставить 
                        //                                                фигуру по правилам тетриса
                        rowscompleted.Add(Program.rowsCompleted(boardTemp));
                        break;
                    }
                }
            }
            Console.WriteLine("Ответ: {0}", rowscompleted.Max());
            Console.Read();
        }

        static void Read(int[,] temp)
        {
            for (int i = 0; i < temp.GetLength(0); i++)
            {
                string tempLine = Console.ReadLine();
                for (int j = 0; j < tempLine.Length; j++)
                {
                    temp[i, j] = (int)tempLine[j] - '0';
                }
            }
        }

        static int GetHeight(int[,] a)
        {
            int height = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (a[i, j] == 1)
                    {
                        height++;
                        break;
                    }
                }
            }
            return height;
        }

        static int GetWidth(int[,] a)
        {
            int width = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (a[j, i] == 1)
                    {
                        width++;
                        break;
                    }
                }
            }
            return width;
        }

        static void rotateFigure(int[,] a)
        {
            int[,] figureTemp = new int[4, 4];
            for (int i = 0; i < GetHeight(a); i++)
                for (int j = 0; j < GetWidth(a); j++)
                    figureTemp[j, GetHeight(a) - i - 1] = a[i, j];
            Array.Copy(figureTemp, a, figureTemp.Length);
        }

        static int rowsCompleted(int[,] a)
        {
            int linesComplete = 0;
            int n = 0;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    if (a[i, j] == 1) n++;
                }
                if (n == a.GetLength(1))
                    linesComplete++;
                n = 0;
            }
            return linesComplete;
        }

        static bool checkOverlay(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    if (a[i, j] == 2) return true;
            return false;
        }

        static void insertFigure(int[,] boardTemp_main, int[,] figure_main, int j_main, int i_main)
        {
            for (int i = 0; i < GetWidth(figure_main); i++)
                for (int j = GetHeight(figure_main) - 1; j >= 0; j--)
                    boardTemp_main[i_main - GetHeight(figure_main) + j, j_main + i] += figure_main[j, i];
        }

    }
}
